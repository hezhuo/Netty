package com.alibaba.demo.dao;

import com.alibaba.demo.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao {
    List<User> findAllUsers();
}
