package com.alibaba.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    @RequestMapping("/main")
    public String main(){
        //每一次http请求 tomcat就会从线程池中调用一个新的线程进行处理。。
        System.out.println(Thread.currentThread().getName());
        System.out.println("hello");
        return "main";
    }


    @InitBinder
    public void init(){
        System.out.println("init");
    }

}
