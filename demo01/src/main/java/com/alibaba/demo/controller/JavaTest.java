package com.alibaba.demo.controller;

import javax.swing.text.ChangedCharSetException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class JavaTest {
    public static void main(String[] args) {
        String string = "abcdefg";

        int i = string.indexOf('b');
        System.out.println(i);
        char c = string.charAt(2);
        System.out.println(c);

        try {
            File tempFile = File.createTempFile("aaa", "txt");
            System.out.println(tempFile);
        } catch (FileNotFoundException e ) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            int s = 1/0;
        } catch (ArithmeticException e) {
            throw  new RuntimeException(e.getMessage());
        }
        System.out.println("heloo");
    }
}
