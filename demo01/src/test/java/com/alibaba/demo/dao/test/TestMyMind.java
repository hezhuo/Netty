package com.alibaba.demo.dao.test;

import com.alibaba.demo.domain.User;

import java.util.HashMap;

public class TestMyMind {

    public static void main(String[] args) {
        User user1 = new User("1","xiaohua","12","1234");
        User user2 = new User("1","xiaohua","12","1234");

        User user3 = new User("133", "xihua", "11", "1234");

        System.out.println(user1.hashCode()+"-----"+user3.hashCode());

        System.out.println("equals:" + user1.equals(user2));
        System.out.println("==:" + (user1==user2));


        HashMap<User, String> map = new HashMap<>();

        map.put(user1,"a");

        map.put(user2,"b");

        map.put(user3,"c");

        //equals返回true 这时候就阻止添加元素了
        //equals返回false，这时候就添加了？

        map.put(user1,"sfesef");

        System.out.println(map);

        System.out.println(user1.hashCode() +" " + user2.hashCode());


    }
}
