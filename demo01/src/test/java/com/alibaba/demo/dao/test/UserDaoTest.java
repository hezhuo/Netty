package com.alibaba.demo.dao.test;


import com.alibaba.demo.dao.UserDao;
import com.alibaba.demo.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-context.xml", "classpath:spring-context-druid.xml", "classpath:spring-context-mybatis.xml"})
public class UserDaoTest {

    @Autowired
    private UserDao tbUserDao;

    @Test
    public void testFindAllUsers() {
        List<User> tbUsers = tbUserDao.findAllUsers();
        for (User tbUser : tbUsers) {
            System.out.println(tbUser.getName());
        }
    }

    @Resource
    DataSource dataSource;

    @Test
    public void testConnection() throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("select * from t_user ");
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            String username = resultSet.getString("username");
            System.out.println(username);
        }
    }


    @Test
    public void test(){
        System.out.println("hello");
    }

}